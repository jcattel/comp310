COMP310: Data Structures
========================

This course will cover all major [data structures](http://www.cs.usfca.edu/~galles/visualization/Algorithms.html) and their [relevance to programming](http://cstheory.stackexchange.com/questions/19759/core-algorithms-deployed/19773#19773). If you're looking for a textbook, check out [Open Data Structures](http://opendatastructures.org/ods-java/).

Agendas
-------

* [January 8](SyllabusCOMP310Lawrance.docx) Introducing ourselves, [Birthday paradox](http://en.wikipedia.org/wiki/Birthday_problem) 
* [January 9](Setup.md) Set up course software and git repositories.
* [January 10](agendas/01-10.md) Add Gradle to your PATH, Abstract Data Types (ADTs), Introducing big O notation.
* [January 13](agendas/01-13.md) Load the workspace into Eclipse, More data structure examples.